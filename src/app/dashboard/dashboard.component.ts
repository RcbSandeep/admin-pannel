import { Component, OnInit } from '@angular/core';
import { ApexAxisChartSeries, ApexChart, ApexDataLabels, ApexLegend, ApexTitleSubtitle, ApexXAxis } from "ng-apexcharts";

export type ChartOptionsBar = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  title: ApexTitleSubtitle;
};



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  chartTitle: ApexTitleSubtitle = {
  };
  legend: ApexLegend = {
    offsetY: 50,
  }
  chartDataLabels: ApexDataLabels = {
    enabled: true,
  };

  chartOptionsBar: any;

  constructor() {
    this.chartOptionsBar = {
      series: [
        {
          name: "My-series",
          data: [10, 30, 40, 20, 60, 70, 50, 80, 75, 65, 70, 55],
        },
      ],
      chart: {
        height: 275,
        type: "bar",
        toolbar: {
          show: false
        },
      },
      xaxis: {
        categories: ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"],
      },
      title: {
        categories: ["Jan", "Feb", "Mar", "Apr"],
      }
    };
  }

  ngOnInit(): void {
  }

}
